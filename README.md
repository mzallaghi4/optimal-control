# Optimal Control



## Getting started


#### Solution methods for optimal control problems: 

- Calculus of Variations
- Pontryagin's Maximum Principle
- Dynamic Programming
- Numerical Optimization
- Model Predictive Control
- Machine Learning 



## Authors 
Mehdi Zallaghi





## Project status
Ongoing
